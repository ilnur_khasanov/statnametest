<?php

namespace App\Jobs;

use App\Lnk;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendReminderEmail implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $lnk;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Lnk $lnk) {
        $this->lnk = $lnk;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        if (!is_null($this->lnk->user)) {
            $user = $this->lnk->user;
            Mail::send(['view' => 'email'], [], function ($m) use ($user) {
                $m->from('bot@statname.net', 'Statename.net');
                $m->to($user->email, $user->name)->subject('Создана короткая ссылка https://q.w/' . $this->lnk->code . ' для ' . $this->lnk->lnk);
            });
        }
    }
}
