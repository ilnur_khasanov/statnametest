<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lnk extends Model {
    protected $table = 'lnks';

    protected $fillable = [
        'user_id', 'lnk', 'code', 'created_at', 'updated_at',
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }
}
