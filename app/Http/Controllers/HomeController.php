<?php

namespace App\Http\Controllers;

use App\Jobs\SendReminderEmail;
use App\Lnk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class HomeController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        return view('home');
    }

    public function lnk(Request $request) {
        $lnk = Lnk::where('lnk', '=', $request->lnk)
            ->where('user_id', '=', Auth::user()->id)
            ->first();

        $validator = Validator::make($request->all(), [
            'lnk' => [
                'required',
                'url',
                Rule::unique('lnks')->where(function ($query) {
                    $query->where('user_id', '=', Auth::user()->id);
                }),
            ],
        ], [
            'lnk.required' => 'Не указана ссылка',
            'lnk.url' => 'Указанное значение должно быть валидным url',
            'lnk.unique' => 'Ссылка уже существует, и ей соответствует: ' . (is_null($lnk) ? '' : 'https://q.w/' . $lnk->code), // todo часть 'https://q.w/' тут по идее префикс ссылок брать с настроек или конфига
        ]);

        if ($validator->fails()) {
            return response()->json([
                'result' => 'error',
                'messages' => $validator->errors()->all(),
            ]);
        }

        $postfix = strval(intval(Lnk::max('id')) + 1); // todo использовать алгоритм сокращения, например переводить в систему счисления повыше или как то иначе

        return response()->json([
            'result' => 'ok',
            'short_lnk' => $postfix,
        ]);
    }

    public function lnkSave(Request $request) {
        $validator = Validator::make($request->all(), [
            'lnk' => 'required|url',
            'lnk_save' => 'required',
        ], [
            'lnk.required' => 'Не указана ссылка',
            'lnk.url' => 'Указанное значение должно быть валидным url',
            'lnk_save.required' => 'Не часть короткой ссылки',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'result' => 'error',
                'messages' => $validator->errors()->all(),
            ]);
        }

        $lnk = Lnk::updateOrCreate([
            'user_id' => Auth::user()->id,
            'lnk' => $request->lnk,
        ], [
            'code' => $request->lnk_save,
        ]);

        $this->dispatch(new SendReminderEmail($lnk));

        return response()->json(['result' => 'ok',]);
    }
}
